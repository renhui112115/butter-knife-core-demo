package com.example.butterknifecoredemo.ioc;

import android.util.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理执行类
 */
public class ListenerInvocationHandler implements InvocationHandler {

    private Object activity;

    private Method activityMethod;

    public ListenerInvocationHandler(Object activity, Method activityMethod) {
        this.activity = activity;
        this.activityMethod = activityMethod;
    }

    /**
     * @param proxy  JDK创建的代理对象
     * @param method 目标类中的方法 (JDK提供)
     * @param args   参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Log.e("111", "ListenerInvocationHandler method = " + method.toString());
        Log.e("111", "activityMethod = " + method.toString());
        return activityMethod.invoke(activity, args);
    }
}
