package com.example.butterknifecoredemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.butterknifecoredemo.ioc.ContentView;
import com.example.butterknifecoredemo.ioc.OnClick;
import com.example.lib_annotations.BindView;

@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @BindView(R.id.button1)
    Button button1;

    @BindView(R.id.button2)
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button1.setText("按钮1");
        button2.setText("按钮2");
    }

    @OnClick({R.id.button1, R.id.button2, R.id.show_dialog})
    public void onclick2(View view) {
        switch (view.getId()) {
            case R.id.button1:
                Toast.makeText(this, "111", Toast.LENGTH_LONG).show();
                break;
            case R.id.button2:
                Toast.makeText(this, "222", Toast.LENGTH_LONG).show();
                break;
            case R.id.show_dialog:
                MainDialog dialog = new MainDialog(this);
                dialog.show();
                break;
        }

    }
}
