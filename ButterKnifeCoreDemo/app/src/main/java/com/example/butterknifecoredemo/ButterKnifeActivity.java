package com.example.butterknifecoredemo;

import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.lib_annotations.BindView;

public class ButterKnifeActivity extends AppCompatActivity {

    @BindView(R.id.button1)
    Button button1;

    @BindView(R.id.button2)
    Button button2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butter_knife);
        Butterknife.bind(this);
        button1.setText("按钮1");
        button2.setText("按钮2");
    }

}
