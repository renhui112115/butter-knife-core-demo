package com.example.butterknifecoredemo;

public interface IBinder<T> {
    void bind(T target);
}