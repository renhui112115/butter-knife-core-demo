package com.example.butterknifecoredemo.ioc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该注解在另外一个注解上使用
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventBase {
    /**
     * 步骤：
     * 1.setOnClickListener 设置订阅关系
     * 2.new View.OnClickListener 创建OnClickListener
     * 3.onClick(View v)  事件处理程序
     **/
    String listenerSetter();

    Class<?> listenerType();

    String callBackMethod();

}
